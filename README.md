# simplesamlphp-module-authoauth2

![maintenance status: unmaintained](https://img.shields.io/maintenance/end%20of%20life/2020)

This is a fork of [simplesamlphp-module-authoauth2 by Cirrus identity](https://github.com/cirrusidentity/simplesamlphp-module-authoauth2),
created to add support for Sign in with Apple.

This fork is now obsolete in favor of [patrickbussmann/oauth2-apple](https://github.com/patrickbussmann/oauth2-apple).
